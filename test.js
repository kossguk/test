//Есть матрица 8*8
//в матрице рандомно расположены числа
//Задача:
//найти сумму самого "дешевого" пути из левой нижней точки в правую верхнюю.
//двигаться можно только вправо или вверх
//за 1 ход можно переместиться только на 1 ячейку.


"use strict";
let board = [
    [1,2,3,4,5,6,7,8], //0
    [1,2,3,4,5,6,7,8], //1
    [1,2,3,4,5,6,7,8], //2
    [1,2,3,4,5,6,7,8], //3
    [1,2,3,4,5,6,7,8], //4
    [1,2,3,4,5,6,7,8], //5
    [1,2,3,4,5,6,7,8], //6
    [1,2,3,4,5,6,7,8], //7
]; //0 1 2 3 4 5 6 7

function getSmallestCostPathFrom (board, startX, startY, targetX, targetY) {

    let minimalPath = {sum: Infinity};

    function recursive(startX, startY, dir, sum) {

        sum += board[startX][startY];

        if (sum > minimalPath.sum) { //optimization
            return;
        }

        let nextYPosition = startY+1,
            nextXPosition = startX-1;

        if (startY === targetY && startX === targetX) {

            dir = dir.substring(0, dir.length - 1);

            let pathObj = {
                path: dir.split(','),
                sum: sum
            };

            if (minimalPath.sum > sum) {
                minimalPath = pathObj;
            }

            return
        }

        if (startY === targetY) {

            recursive(nextXPosition, startY, dir + 'up,', sum);
            return;
        }

        if (startX === targetX) {
            recursive(startX, nextYPosition, dir + 'right,', sum);
            return;
        }

        recursive(startX, nextYPosition, dir + 'right,', sum);
        recursive(nextXPosition, startY, dir + 'up,', sum);
    }

    recursive(startX, startY, '', 0);

    return minimalPath;

}

console.log(getSmallestCostPathFrom(board,board.length-1, 0, 0, board[0].length-1));